// 分销
export const my_distributionList = [{
  id: 0,
  name: "我的团队",
  icon: 'iconfont icon-tuandui',
  color: '#22a4f1',
  path: '/pages/my_team/my_team'
}, {
  id: 1,
  name: "分销订单",
  icon: 'iconfont icon-dingdan',
  color: '#006fb2',
  path: '/pages/distribution_order/distribution_order'
}, {
  id: 2,
  name: "邀请好友",
  icon: 'iconfont icon-yaoqing',
  color: '#dd4f42',
  path: '/pages/invitation/invitation'
}]

// 首页
// tabs
const indexTabsList = [{
    id: 0,
    name: "推荐课程",
    type: 2
  },
  {
    id: 1,
    name: "精选课程",
    type: 1
  },
  {
    id: 2,
    name: "专项课程",
    type: 3
  },
  {
    id: 3,
    name: "免费课程",
    type: 4
  },
  {
    id: 4,
    name: "拼团课程",
    type: 6
  },
]
// 导航
const indexNavList = [{
    id: 0,
    name: "拼团报课",
    url: "/static/images/rzhy.png",
    path: '/pages/spell_group/spell_group',
    params: {}
  },
  {
    id: 1,
    name: "免费好课",
    url: "/static/images/mfhk.png",
    path: '/pages/search/search',
    params: {
      type: 4
    }
  },
  // type 2,推荐课程；1,精选课程;3,专项课程; 4,免费课程 6,拼团课程
  // playType 1：点播 2：直播 （课程专属）3:随身听(音频)',
  {
    id: 2,
    name: "直播课程",
    url: "/static/images/zbkc.png",
    path: '/pages/search/search',
    params: {
      playType: 2
    }
  },
  {
    id: 3,
    name: "模拟试题",
    url: "/static/images/mnst.png",
    params: {}
  },
  {
    id: 4,
    name: "书籍专栏",
    url: "/static/images/sjzl.png",
    path: '/pages/tabbar/tab_index/more',
    params: {
      title: '书籍列表'
    }
  },
]

// 我的学习
const userCourseTabs = [{
    id: 0,
    value: "点播",
  },
  {
    id: 1,
    value: "直播",
  },
  {
    id: 2,
    value: "随身听",
  },
]

// 网络类型
const netWorkStateLits = ['wifi', '2g', '3g', '4g', '5g', 'ethernet', 'unknown',
  'none'
]
export const courseDetailTabs = [{
  name: '详情',
}, {
  name: '目录',
}, {
  name: '评价'
}]

// 1 错题本 2 题目收藏 3 最近练习
export const exerciseList = [{
  name: '错题本',
}, {
  name: '题目收藏',
}, {
  name: '最近练习'
}]

export const goldPayList = [{
    gold: 6,
    money: 4.2,
  },
  {
    gold: 18,
    money: 12.6,
  },
  {
    gold: 30,
    money: 21.0,
  },
  {
    gold: 68,
    money: 47.6,
  },
  {
    gold: 98,
    money: 68.6,
  },
  {
    gold: 128,
    money: 89.6,
  },
]

export const userProblemTabs = [{
    id: 0,
    name: "章节练习",
  },
  {
    id: 1,
    name: "在线评估",
  },
]

export default {
  indexTabsList,
  indexNavList,
  userCourseTabs,
  netWorkStateLits
}
