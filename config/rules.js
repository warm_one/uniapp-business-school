// token 白名单
const tokenWhiteList = ["/login", "/register"];

// 路由白名单
const routerWhiteList = [
  "/pages/tabbar/tab_index/tab_index",
  "/pages/tabbar/tab_myself/tab_myself",
  "/pages/login/login",
];

export default {
  tokenWhiteList,
  routerWhiteList,
};
