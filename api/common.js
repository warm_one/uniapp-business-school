import request from "../request"

// 收藏课程
export const setCollec = (data) => {
  return request({
    url: "/collec/bool",
    method: "POST",
    data,
  })
}

// 收藏题目
export const setCollecQuestion = (data) => {
  return request({
    url: "/collec/question/bool",
    method: "POST",
    data,
  })
}

// 正在拼团的成员 level=6时调用
export const getGroupPerson = (data) => {
  return request({
    url: '/group/groupPerson',
    method: "POST",
    data,
  });
};

// 收获地址列表
export const userReceiveInfoList = (data) => {
  return request({
    url: '/userReceiveInfo/list',
    method: "GET",
    data,
  });
};

// 保存or修改 地址
export const saveOrUpdate = (data) => {
  return request({
    url: '/userReceiveInfo/saveOrUpdate',
    method: "POST",
    data,
  });
};

// 设置默认地址
export const setDefault = (data) => {
  return request({
    url: '/userReceiveInfo/setDefault',
    method: "POST",
    data,
  });
};

// 意见反馈
export const submitFeedBack = (data) => {
  return request({
    url: '/feedback/submitFeedBack',
    method: "POST",
    data,
  });
};


// 分销
// 分销首页
export const distributionCoreIndex = (data) => {
  return request({
    url: '/distributionCore/index',
    method: "GET",
    data,
  });
};
// 我的团队
export const distributionCoreMyTeam = (data) => {
  return request({
    url: '/distributionCore/myTeam',
    method: "GET",
    data,
  });
};

// 分销订单
export const distributionCoreOrder = (data) => {
  return request({
    url: '/distributionCore/order',
    method: "GET",
    data,
  });
};

// 我的团队-成员邀请
export const distributionCoreMyTeamInvite = (data) => {
  return request({
    url: '/distributionCore/myTeamInvite',
    method: "GET",
    data,
  });
};

// 获取二维码base64 type	 0分销分享 1课程分享
export const weiXinMinAppGetUnlimited = (data) => {
  return request({
    url: '/weiXinMinApp/getUnlimited',
    method: "GET",
    data,
  });
};
