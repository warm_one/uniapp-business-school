import request from "../request";

// 判断手机号是否已经注册
export function checkPhone(data) {
  return request({
    url: "/account/checkPhone",
    method: "POST",
    data,
  });
}

// 登录接口，手机+短信验证码登录
export function doLoginByPhone(data) {
  return request({
    url: "/account/doLoginByPhone",
    method: "POST",
    data,
  });
}

// 根据类型发送短信 测试默认为123456
export function sendByType(data) {
  return request({
    url: "/sendSMS/sendByType",
    method: "POST",
    data,
  });
}

// 注册接口,手机号+验证码,注册成功直接登录
export function doRegister(data) {
  return request({
    url: "/account/doRegister",
    method: "post",
    data,
  });
}

// 更换手机号
export function changePhone(data) {
  return request({
    url: "/userinfo/changePhone",
    method: "post",
    data,
  });
}

// 输入邀请码
export function saveInviteCode(data) {
  return request({
    url: "/userinfo/saveInviteCode",
    method: "post",
    data,
  });
}

// 完善资料(修改头像等信息) avatarPath  nickName
export function savePersonalInfo(data) {
  return request({
    url: "/userinfo/savePersonalInfo",
    method: "post",
    data,
  });
}

// 获取自身信息
export function getPersonalInfo(data) {
  return request({
    url: "/userinfo/getPersonalInfo",
    method: "GET",
    data,
  });
}

// 退出
// export function logout() {
//   return request({
//     url: "logout",
//     method: "post",
//   })
// }
