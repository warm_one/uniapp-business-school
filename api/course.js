import request from "../request"

// 首页课程
export function homePageIndex(data) {
  return request({
    url: "/homePage/index",
    method: "GET",
    data,
  })
}

// 课程大分类
export function majorBigList() {
  return request({
    url: "/major/bigList",
    method: "GET",
  })
}

// 小分类
export function majorList(data) {
  return request({
    url: "/major/list",
    method: "GET",
    data,
  })
}

// 根据课程分类获取课程列表
export function coursePackageList(data) {
  return request({
    url: "/coursePackage/list",
    method: "GET",
    data,
  })
}


// 获取课程简介
export function getIntroduce(data) {
  return request({
    url: "/coursePackage/getIntroduce",
    method: "GET",
    data,
  })
}

// 获取课程目录
export const getSection = async (data) => {
  let res = await request({
    url: '/coursePackage/section/list',
    data,
  });
  return res;
};

// 获取课程下的评论列表
export const getCommentList = async (data) => {
  let res = await request({
    url: '/comment/getCommentList',
    data,
  });
  return res;
};

// 课程评论
export const sendComment = (data) => {
  return request({
    url: '/comment/sendComment',
    data,
    method: 'POST',
  });
};


// 播放一个视频或者音频,获取播放播放地址
export const playSectionVideo = (data) => {
  return request({
    url: '/coursePackage/playSectionVideo',
    data,
    method: 'GET',
  });
};

// 进入课程简介 直接播放(直播和回放都用一个接口)
export const liveInchapter = (data) => {
  return request({
    url: '/coursePackage/liveInchapter',
    data,
    method: 'GET',
  });
};


// 进入课程简介 直接点击播放(点播和音频可调用此接口)
export const freeSectionPlayInchapter = (data) => {
  return request({
    url: '/coursePackage/freeSectionPlayInchapter',
    data,
    method: 'GET',
  });
};

// 拼团------------
// 发起 拼团 购买(金币又名积分)   coursePackageId	课程id
export const createGroup = (data) => {
  return request({
    url: '/userBuyCourseRecord/createGroup',
    data,
    method: 'POST',
  })
}
// 直接 购买(金币又名积分)  id	商品id (包含课程id 直播id)  type类型 0课程 1直播
export const goldBuyGoods = (data) => {
  return request({
    url: '/userBuyCourseRecord/goldBuyGoods',
    data,
    method: 'POST',
  })
}
// 加入 拼团 购买(金币又名积分)  coursePackageId	课程id    userId	用户id 团长id
export const joinGroup = (data) => {
  return request({
    url: '/userBuyCourseRecord/joinGroup',
    data,
    method: 'POST',
  })
}

// 是否预约 (直播)
export const makeAppointBool = (data) => {
  return request({
    url: '/makeAppoint/bool',
    data,
    method: 'POST',
  })
}

// 我的课程
export function userinfoCourseList(data) {
  return request({
    url: "/userinfo/my/course",
    method: "GET",
    data,
  })
}


// 我的收藏
export function userinfoCollect(data) {
  return request({
    url: "/userinfo/my/collect",
    method: "GET",
    data,
  })
}

// 我的书籍
export function userinfoGoods(data) {
  return request({
    url: "/userinfo/my/goods",
    method: "GET",
    data,
  })
}

// 我的做题
export function userinfoQuestion(data) {
  const res = request({
    url: "/userinfo/my/question",
    method: "GET",
    data,
  })
  return res
}

// 章节练习列表
export function chapterExerciseList(data) {
  return request({
    url: "/makeQuestion/chapterExercise/list",
    method: "GET",
    data,
  })
}

// 根据章节练习 获取题目
export function chapterExerciseQuestion(data) {
  return request({
    url: "/makeQuestion/chapterExercise/question/get",
    method: "GET",
    data,
  })
}

// 章节练习 -提交或者保存
export function chapterExerciseQuestionSubmit(data) {
  data.contentType = "application/json"
  return request({
    url: "/makeQuestion/chapterExercise/question/submit",
    method: "POST",
    data
  })
}

// 在线测评 提交
export function onlineEvaluationQuestionSubmit(data) {
  data.contentType = "application/json"
  return request({
    url: "/makeQuestion/onlineEvaluation/question/submit",
    method: "POST",
    data,
  })
}

// 错题本 题目收藏和最近练习 1 错题本 2 题目收藏 3 最近练习,
export function questionTypeList(data) {
  return request({
    url: "/makeQuestion/questionType/list",
    method: "GET",
    data,
  })
}

// 根据错题本和题目收藏 获取题目
export function getQuestionTypeList(data) {
  return request({
    url: "/makeQuestion/questionType/question/get",
    method: "GET",
    data,
  })
}


// 根据最近练习 获取题目
export function getQuestionRecord(data) {
  return request({
    url: "/makeQuestion/questionType/questionRecord/get",
    method: "GET",
    data,
  })
}
