import request from "../request"

// 首页banner列表
export function homePageBanner(data) {
  return request({
    url: "/homePage/banner/list",
    method: "get",
    data,
  })
}
