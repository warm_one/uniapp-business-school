import request from "../request"

// 获取书籍列表
export function getBookList(data) {
  return request({
    url: "/commodityInfo/list",
    method: "GET",
    data,
  })
}

// 获取书籍详情
export function getBookDetail(data) {
  return request({
    url: "/commodityInfo/getIntroduce",
    method: "GET",
    data,
  })
}
