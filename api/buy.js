import request from "../request";

// 直接 购买(金币又名积分)
export function goldBuyGoods(data) {
  return request({
    url: "/userBuyCourseRecord/goldBuyGoods",
    method: "POST",
    data,
  });
}

// 获取充值项目
export function listRechargeInfo(data) {
  return request({
    url: "/recharge/listRechargeInfo",
    method: "POST",
    data,
  });
}
// 列表充值账单
export function listBill(data) {
  return request({
    url: "/recharge/listBill",
    method: "POST",
    data,
  });
}

// 充值-微信支付
// 统一下单
export function wxpayCreateOrder(data) {
  return request({
    url: "/recharge/wxpay/createOrder",
    method: "POST",
    data,
  });
}
// 支付回调通知处理
export function wxpayNotifyOrder(data) {
  return request({
    url: "/recharge/wxpay/notify/order",
    method: "POST",
    data,
  });
}
// 退款回调通知处理
export function wxpayNotifyRefund(data) {
  return request({
    url: "/recharge/wxpay/notify/refund",
    method: "POST",
    data,
  });
}

// 支付宝
// 统一下单
export function alipayCreateOrder(data) {
  return request({
    url: "/recharge/alipay/createOrder",
    method: "POST",
    data,
  });
}
// 支付回调通知处理
export function alipayNotifyOrder(data) {
  return request({
    url: "/recharge/alipay/notify/order",
    method: "POST",
    data,
  });
}
