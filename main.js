import App from "./App.vue";
import { formatTime } from "@/utils/util";
// 引入全局存储
import store from "@/store";
import { showToast, showModal } from "@/utils/util.js";
import { getStorageItem } from "@/utils/storage.js";
import { Token } from "@/utils/constant.js";
import rules from "@/config/rules";

// 获取网络类型。
uni.getNetworkType({
  success: (res) => {
    if (res.errMsg === "getNetworkType:ok") {
      store.dispatch("equipment/setNetworkState", res.networkType);
    }
  },
});
// 网络状态监听
uni.onNetworkStatusChange(function (res) {
  store.dispatch("equipment/setNetworkState", res.networkType);
});

// #ifndef VUE3
import Vue from "vue";
Vue.filter("formatTime", function (date) {
  return formatTime(date);
});
Vue.config.productionTip = false;
App.mpType = "app";

import uView from "uview-ui";
Vue.use(uView);

// 路由拦截
const routerList = rules.routerWhiteList;
Vue.prototype.$u.routeIntercept = (params, resolve) => {
  if (routerList.includes(params.url)) {
    resolve(true);
  } else {
    const token = getStorageItem(Token) || "";
    if (token) {
      resolve(true);
    } else {
      resolve(false);
      showModal({
        content: "当前账号未登录，请登陆后进行操作",
      }).then((res) => {
        if (!res) {
        } else {
          uni.navigateTo({
            url: "/pages/login/login",
          });
        }
      });
    }
  }
};

Vue.prototype.$ShowToast = showToast;
Vue.prototype.$ShowModal = showModal;

const app = new Vue({
  store,
  ...App,
});
app.$mount();

// #endif

// #ifdef VUE3
import { createSSRApp } from "vue";
export function createApp() {
  const app = createSSRApp(App);
  return {
    app,
  };
}
// #endif
