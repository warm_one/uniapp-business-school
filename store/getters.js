const getters = {
  maxWidth: (state) =>
    state.equipment.menuButtonInfo.left ||
    state.equipment.SystemInfo.safeArea.width,

  // padRight: (state) => (state.equipment.safeArea.width - state.equipment
  //   .menuButtonInfo.left) || 0,

  headerHeight: (state) =>
    ((state.equipment.menuButtonInfo.top - state.equipment.SystemInfo
      .statusBarHeight) * 2 + state.equipment.menuButtonInfo.height) || 44,

  navHeight: state =>
    (state.equipment.SystemInfo.statusBarHeight || 0) + ((state.equipment
      .menuButtonInfo.top - state.equipment.SystemInfo
      .statusBarHeight) * 2 + state.equipment.menuButtonInfo.height) || 0

}
export default getters
