import {
  getStorageItem,
  setStorageItem,
  removeStorageItem,
} from "@/utils/storage.js"

import {
  SystemInfo,
  MenuButtonInfo
} from "@/utils/constant"

const state = {
  SystemInfo: getStorageItem(SystemInfo) || {}, // 设备信息
  menuButtonInfo: getStorageItem(MenuButtonInfo) || {}, // 微信小程序胶囊位置
  networkState: '', // 网络状态
  networkStateChange: {}, // 变化后的网络状态
}

const mutations = {
  SET_SYSTEMINFO: (state, res) => {
    state.SystemInfo = res
  },
  SET_MENUBUTTONINFO: (state, res) => {
    state.menuButtonInfo = res
  },
  SET_NETWORKSTATE: (state, res) => {
    state.networkState = res
  },
  SET_NETWORKSTATECHANGE: (state, res) => {
    state.networkStateChange = res
  },
}

const actions = {
  setSystemInfo({
    commit
  }, res) {
    commit("SET_SYSTEMINFO", res)
  },
  setMenuButtonInfo({
    commit
  }, res) {
    commit('SET_MENUBUTTONINFO', res)
  },
  setNetworkState({
    commit
  }, res) {
    commit('SET_NETWORKSTATE', res)
  },
  networkStateChange({
    commit
  }, res) {
    commit('SET_NETWORKSTATECHANGE', res)
  },

}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
