import urls from "@/config"
import {
  showToast
} from "@/utils/util"

// 获取用户的当前设置。返回值中只会出现小程序已经向用户请求过的权限。
export const getSetting = () => {
  return new Promise((resolve, reject) => {
    uni.getSetting({
      success: (res) => {
        resolve(res.authSetting)
      },
      fail: (err) => {
        console.log(err)
      },
    })
  })
}

// 调起客户端小程序设置界面，返回用户设置的操作结果。设置界面只会出现小程序已经向用户请求过的权限。
export const openSetting = () => {
  return new Promise((resolve, reject) => {
    uni.openSetting({
      success: (res) => {
        resolve(res.authSetting)
      },
      fail: (err) => {
        console.log(err)
      },
    })
  })
}

// 动态设置导航栏颜色
export const setNavigationBarColor = (deta) => {
  // 仅接收一个 backgroundColor
  uni.setNavigationBarColor({
    frontColor: "#ffffff", // 前景颜色值，包括按钮、标题、状态栏的颜色，仅支持 #ffffff 和 #000000
    backgroundColor: deta.backgroundColor, // 背景颜色值，有效值为十六进制颜色
    animation: {
      duration: 0,
      timingFunc: "linear",
    },
  })
}

// 动态设置当前页面的标题
export const setNavigationBarTitle = (title) => {
  uni.setNavigationBarTitle({
    title,
  })
}

// 下载文件资源到本地  客户端直接发起一个 HTTPS GET 请求，返回文件的本地临时路径 (本地路径)，单次下载允许的最大文件为 200MB
export const downloadFile = () => {
  // return new Promise((resolve, reject) => {
  // let url = +
  uni.downloadFile({
    url,
    type: "image",
    success: (res) => {
      if (res.statusCode === 200) {
        // 保存到系统相册
        if (uni.saveImageToPhotosAlbum) {
          uni.saveImageToPhotosAlbum({
            filePath: res.tempFilePath,
            success: (res) => {
              uni.showToast({
                title: "已下载至相册",
                duration: 1500,
                mask: true,
              })
              console.log("下载成功")
            },
          })
        } else {
          // 如果希望用户在最新版本的客户端上体验您的小程序，可以这样子提示
          uni.showModal({
            title: "提示",
            content: "当前微信版本过低，无法使用该功能，请升级到最新微信版本后重试。",
          })
        }
      } else {
        uni.showToast({
          title: "网络异常",
          // image: '/images/search_no.png',
          duration: 2000,
          mask: true,
        })
      }
    },
  })
  // })
}

// 将本地资源上传到服务器。客户端发起一个 HTTPS POST 请求，其中 content-type 为 multipart/form-data
export const chooseImage = (data) => {
  return new Promise((resolve, reject) => {
    uni.chooseImage({
      count: data.count,
      sizeType: ["original", "compressed"],
      sourceType: ["album", "camera"],
      success: (res) => {
        const tempFilePaths = res.tempFilePaths
        console.log(tempFilePaths)
        let l = urls.baseUrl + "uploadFile" // 图片上传路径
        let uploadImageList = []
        for (let i = 0; i < tempFilePaths.length; i++) {
          uni.uploadFile({
            url: l,
            filePath: tempFilePaths[i],
            name: "file",
            formData: {
              user: "test",
            },
            success: (res) => {
              let e = res.data
              if (e.substring(e.length - 1) == '"') {
                e = e.substr(1, e.length - 2)
              }
              uploadImageList.push(e)
            },
          })
          if (i == tempFilePaths.length - 1) {
            resolve(uploadImageList)
          }
        }
      },
    })
  })
}

// 拍摄视频或从手机相册中选 视频。
export const chooseVideo = () => {
  return new Promise((resolve, reject) => {
    uni.chooseVideo({
      sourceType: ["album", "camera"],
      maxDuration: 60, // 视频最大时间
      camera: "back", // 默认拉起后摄像头
      success: (res) => {
        console.log(res.tempFilePath)
        resolve(res.tempFilePath)
      },
    })
  })
}

// 拍摄或从手机相册中选择  图片  或  视频。
export const chooseMedia = (data) => {
  return new Promise((resolve, reject) => {
    uni.chooseMedia({
      count: data.count,
      mediaType: ["image", "video"],
      sourceType: ["album", "camera"],
      maxDuration: 30, // 拍摄视频最长拍摄时间，单位秒。时间范围为 3s 至 30s 之间
      camera: "back",
      success(res) {
        resolve(res.tempFiles)
        console.log(res.tempFiles)
      },
    })
  })
}

// 预览图片
export const previewImage = (data) => {
  console.log(data)
  let current = data.index
  let imgList = data.imgList || []
  if (imgList.length !== 0) {
    if (!imgList[0].includes("https://") || !imgList[0].includes("http://")) {
      // 如果图片路径不是以https开头
      for (let i = 0; i < imgList.length; i++) {
        imgList[i] = urls.baseUrl + imgList[i]
      }
    }
    uni.previewImage({
      current,
      urls: imgList,
    })
  } else {
    // 没有传图片路径
    showToast({
      title: "请传图片！",
    })
  }
}

// 获取菜单栏的位置
export const getMenuButton = () => {
  /*
    width    宽度，单位：px
    height   高度，单位：px
    top    上边界坐标，单位：px
    right     右边界坐标，单位：px
    bottom    下边界坐标，单位：px
    left    左边界坐标，单位：px
  */
  return uni.getMenuButtonBoundingClientRect()
}
