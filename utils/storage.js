import {
  showToast,
  showModal
} from './util.js'

export const setStorageItem = (key, value) => {
  try {
    uni.setStorageSync(key, value)
  } catch (error) {
    // error
    console.log(error);
  }

}

export const getStorageItem = key => {
  try {
    return uni.getStorageSync(key)
  } catch (error) {
    // error
    console.log(error);
  }
}

export const getStorageInfo = () => {
  try {
    const res = uni.getStorageInfoSync();
    console.log(res);
  } catch (e) {
    // error
  }
}

export const removeStorageItem = key => {
  uni.removeStorage({
    key,
    success: () => {
      showToast({
        title: '删除成功！'
      })
    },
    fail: () => {
      showToast({
        title: '删除失败，请重新尝试！'
      })
    }
  });
}

export const clearStorage = key => {
  showModal({
    content: '清除所有缓存可能影响当前使用，需要重启程序，是否继续？',
    showCancel: true
  }).then(() => {
    uni.clearStorage();
  })
}
